topDesFileName = 'C:\\Users\\Igor\\Documents\\LO RTM for ESS\\Sub-projects\\top_des.csv';
topTrackFileName = 'C:\\Users\\Igor\\Documents\\LO RTM for ESS\\Sub-projects\\top_track.csv';  

bottomDesFileName = 'C:\\Users\\Igor\\Documents\\LO RTM for ESS\\Sub-projects\\bottom_des.csv';
bottomTrackFileName = 'C:\\Users\\Igor\\Documents\\LO RTM for ESS\\Sub-projects\\bottom_track.csv';   
 

function writeTopOverlayData() {
    writeOverlayData(eTopOverlay,topDesFileName,topTrackFileName);
}

function writeBottomOverlayData() {
    writeOverlayData(eBottomOverlay,bottomDesFileName,bottomTrackFileName);
}

function readTopOverlayData() {
    readOverlayData(eTopOverlay,topDesFileName,topTrackFileName);
}

function readBottomOverlayData() {
    readOverlayData(eBottomOverlay,bottomDesFileName,bottomTrackFileName);
}

function writeOverlayData(layer_selection, desFileName, trackFileName) {

    if(PCBServer == null) {
      ShowMessage("No PCB Server!");
      return;
      }
    var Board = PCBServer.GetCurrentPCBBoard();
    if(Board == null){
      ShowMessage("No PCB Board!");
      return;
    }
    //ShowMessage(Board.FileName);

    var Iterator = Board.BoardIterator_Create;
    if(Iterator == null){
      ShowMessage('No iterator!');
      return;
    }
    Iterator.AddFilter_ObjectSet(MkSet(eTextObject));
    Iterator.AddFilter_LayerSet(MkSet(layer_selection));
    //Iterator.AddFilter_Method(eProcessAll);
    //Iterator.AddFilter_ObjectSet(MkSet(eComponentObject));
    //Iterator.AddFilter_LayerSet(MkSet(eTopLayer));

    desList = TStringList.Create();
    var count = 0;

    var Designator_It = Iterator.FirstPCBObject;
    while(Designator_It != null){
      if (Designator_It.isDesignator() == 1) {
        test_str =  extendString(String(Designator_It.Text),20);
        x_str =     extendString(String(Designator_It.XLocation),20);
        y_str =     extendString(String(Designator_It.YLocation),20);
        rot_str=    extendString(String(Designator_It.Rotation),20);
        desList.Add(test_str + ',' + x_str + ',' + y_str +',' + rot_str);
        count = count + 1;
      }
      Designator_It = Iterator.NextPCBObject;
    }
    //ShowMessage(count.toString());
    desList.SaveToFile(desFileName);
    Board.BoardIterator_Destroy(Iterator);
    desList.Free

    var Iterator = Board.BoardIterator_Create;
    count = 0;
    if(Iterator == null){
      ShowMessage('No iterator!');
      return;
    }
    trackList = TStringList.Create();
    Iterator.AddFilter_ObjectSet(MkSet(eTrackObject));
    Iterator.AddFilter_LayerSet(MkSet(layer_selection));
    var Track_It = Iterator.FirstPCBObject;
    while(Track_It != null){
      if (Track_It.IsFreePrimitive() == 1) {
        x1_str =     extendString(String(Track_It.X1),20);
        y1_str =     extendString(String(Track_It.Y1),20);
        x2_str =     extendString(String(Track_It.X2),20);
        y2_str =     extendString(String(Track_It.Y2),20);
        w_str  =     extendString(String(Track_It.Width),20);
        trackList.Add(x1_str + ',' + y1_str + ',' + x2_str + ',' + y2_str + ',' + w_str);
        count = count + 1;
      }
      Track_It = Iterator.NextPCBObject;
    }
    //ShowMessage(count.toString());
    trackList.SaveToFile(trackFileName);
    Board.BoardIterator_Destroy(Iterator);
    trackList.Free
}

function readOverlayData(layer_selection, desFileName, trackFileName) {

    if(PCBServer == null)
        return;
    var Board = PCBServer.GetCurrentPCBBoard();
    if(Board == null)
        return;
    ShowMessage(Board.FileName);

    strList = TStringList.Create();
    strList.LoadFromFile(desFileName)

    desTupleList = [];
    for(i = 0; i < strList.Count; i++) {
    //for(i = 0; i < 2; i++) {
      tupleObj = {};
      tupleObj.des  =  trim(strList(i).substring(0,20));
      tupleObj.x    =  Number(strList(i).substring(21,41));
      tupleObj.y    =  Number(strList(i).substring(42,62));
      tupleObj.rot  =  Number(strList(i).substring(63,83));
      //ShowMessage(tupleObj.des);
      //ShowMessage(tupleObj.x);
      //ShowMessage(tupleObj.y);
      desTupleList.push(tupleObj);
    }
    strList.Free

    strList = TStringList.Create();
    strList.LoadFromFile(trackFileName)
    trackTupleList = [];
    for(i = 0; i < strList.Count; i++) {
    //for(i = 0; i < 2; i++) {
      tupleObj = {};
      tupleObj.x1    =  Number(strList(i).substring(0,20));
      tupleObj.y1    =  Number(strList(i).substring(21,41));
      tupleObj.x2    =  Number(strList(i).substring(42,62));
      tupleObj.y2    =  Number(strList(i).substring(63,83));
      //ShowMessage(tupleObj.x1);
      //ShowMessage(tupleObj.y1);
      //ShowMessage(tupleObj.x2);
      //ShowMessage(tupleObj.xy);
      trackTupleList.push(tupleObj);
    }
    strList.Free

    var Iterator = Board.BoardIterator_Create;
    if(Iterator == null){
      ShowMessage('No iterator!');
      return;
    }
    Iterator.AddFilter_ObjectSet(MkSet(eTextObject));
    Iterator.AddFilter_LayerSet(MkSet(layer_selection));
    var count = 0;


    PCBServer.PreProcess;
    var Designator_It = Iterator.FirstPCBObject;
    while(Designator_It != null){
      if (Designator_It.isDesignator() == 1) {
        tuple = findTuple(desTupleList,Designator_It.Text);
        if (tuple != null) {
          Designator_It.XLocation = tuple.x;
          Designator_It.YLocation = tuple.y;
          Designator_It.Rotation  = tuple.rot;
        }
        count = count + 1;
      }
      Designator_It = Iterator.NextPCBObject;
    }
    ShowMessage(count)

    count = 0
    for(i = 0; i < trackTupleList.length; i++){
    //for(i = 0; i < 2; i++){
      trackObj = trackTupleList[i];
      pcbTrack = findTrack(trackObj.x1,trackObj.y1,trackObj.x2,trackObj.y2,layer_selection);
      if (pcbTrack != null)
        count = count + 1;
      else
      {
        Track = PCBServer.PCBObjectFactory(eTrackObject, eNoDimension, eCreate_Default);
        Track.X1 = trackObj.x1;
        Track.Y1 = trackObj.y1;
        Track.X2 = trackObj.x2;
        Track.Y2 = trackObj.y2;
        Track.Layer = layer_selection;
        var Width = 100000;
        //StringToCoordUnit('10mil',Width,Board.DisplayUnit)
        Track.Width = Width;
        Board.AddPCBObject(Track)
        ShowMessage('Track added')
      }
    }
    ShowMessage(count)
    PCBServer.PostProcess;
    Board.BoardIterator_Destroy(Iterator);
    AddStringParameter("Action", "Redraw");
    RunProcess("PCB:Zoom");
}

function extendString(str, len) {

  extendBy = len - str.length;
  if(extendBy < 0){
    ShowMessage('Can\'t shorten the string');
    return str;
  }
  strFin = str;
  for(i = 0; i < extendBy; i++)
    strFin = strFin + ' ';

  return strFin;
}

function findTuple(tupleArray, designator) {
  for(i = 0; i < tupleArray.length; i++){
    if (tupleArray[i].des.localeCompare(designator) == 0)
      return tupleArray[i];
  }
  ShowMessage('Designator not found in array of tuples ');
  ShowMessage(designator);
}

function findTrack(x1,y1,x2,y2,layer_selection) {
    //ShowMessage(String(x1))
    //ShowMessage(String(y1))
    //ShowMessage(String(x2))
    //ShowMessage(String(y2))
    if(PCBServer == null) {
        ShowMessage("No PCB Server!");
        return;
    }
    var Board = PCBServer.GetCurrentPCBBoard();
    if(Board == null){
        ShowMessage("No PCB Board!");
        return;
    }
    var Iterator = Board.BoardIterator_Create;
    if(Iterator == null){
      ShowMessage('No iterator!');
      return;
    }
    Iterator.AddFilter_ObjectSet(MkSet(eTrackObject));
    Iterator.AddFilter_LayerSet(MkSet(layer_selection));
    var Track_It = Iterator.FirstPCBObject;
    if(Track_It == null){
      ShowMessage('No track iterator!');
      return;
    }
    //ShowMessage('Iterating');
    trackSearchCount = 0;
    while(Track_It != null){
      trackSearchCount = trackSearchCount + 1;
      if (Track_It.X1 == x1 && Track_It.Y1 == y1 && Track_It.X2 == x2 &&Track_It.Y2 == y2)
      {
        Board.BoardIterator_Destroy(Iterator);
        //ShowMessage('Track found in func')
        return Track_It;
      }
      Track_It = Iterator.NextPCBObject;
    }
    //ShowMessage(String(trackSearchCount));
    Board.BoardIterator_Destroy(Iterator);
    return null;
}

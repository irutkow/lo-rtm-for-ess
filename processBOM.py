# -*- coding: utf-8 -*-

import pandas 


BOM = pandas.read_excel('./Project Outputs for LO_RTM/BOM/Bill of Materials-LO_RTM.xls')
BOM.info()

capsSelection =  BOM['Designator'].str.match('[C][0-9]+') == True 
connSelection =  BOM['Designator'].str.match('[J][0-9]+') == True  
indSelection  =  BOM['Designator'].str.match('[L][0-9]+') == True  
resSelection  =  (BOM['Designator'].str.match('[R][0-9]+') == True) & BOM['Manufacturer'].isnull()

print('Capacitors')
capsDf = BOM.loc[capsSelection,:].copy()
BOM.drop(BOM.loc[capsSelection,:].index,inplace=True)
capsDf.drop(columns =['Manufacturer','ManufacturerNr','Comment','Description'],inplace=True)
vaultCaps = capsDf['Footprint'].str.startswith('CAPC')
capsDf.loc[vaultCaps,'Footprint'] = capsDf.loc[vaultCaps,'Footprint'].str[4:]
capsDf.Tolerance.replace({r'[^\x00-\x7F]+':''}, regex=True, inplace=True)
capsDf['Tolerance'] = '±' + capsDf['Tolerance']
capsDf.sort_values(['Footprint','Value'],inplace=True)
print(capsDf)
print()

print('Connectors')
connDf = BOM.loc[connSelection,:].copy()
BOM.drop(BOM.loc[connSelection,:].index,inplace=True)
connDf.drop(columns =['Rated Voltage','Tolerance','Value','Comment','Footprint'],inplace=True)
connDf = connDf[['Manufacturer','ManufacturerNr','Quantity','Designator','Description']]
print(connDf)
print()

print('Inductors')
indDf = BOM.loc[indSelection,:].copy()
BOM.drop(BOM.loc[indSelection,:].index,inplace=True)
indDf.drop(columns =['Rated Voltage','Tolerance','Footprint','Value','Description','Comment'],inplace=True)
print(indDf)
print()

print('Resistors')
resDf  = BOM.loc[resSelection,:].copy()
BOM.drop(BOM.loc[resSelection, :].index,inplace=True)
resDf.drop(columns =['Manufacturer','ManufacturerNr','Rated Voltage','Description','Comment'],inplace=True)
vaultRs = resDf['Footprint'].str.startswith('RESC')
resDf.loc[vaultRs,'Footprint'] = resDf.loc[vaultRs,'Footprint'].str[4:]
resDf.sort_values(['Footprint','Value'],inplace=True)
#resDf = resDf[['Footprint','Tolerance','Value','Quantity','Designator']]
print(resDf)
print()

print('Main')
selector1 = BOM['ManufacturerNr'].isnull() & (BOM['Comment'].isnull()==False)
BOM.loc[selector1,'ManufacturerNr'] = BOM.loc[selector1,'Comment']
BOM.drop(columns =['Rated Voltage','Tolerance','Value','Comment','Footprint'],inplace=True)
print(BOM)
#print(BOM.loc[BOM['ManufacturerNr'].isnull()==True,:])
BOM.info()


writer = pandas.ExcelWriter('processed BOM for comparison.xlsx', engine='xlsxwriter')
BOM.to_excel(writer, sheet_name='Main',index =False)
capsDf.to_excel(writer, sheet_name='Caps',index =False)
connDf.to_excel(writer, sheet_name='Connectors',index =False)
indDf.to_excel(writer, sheet_name='Ind',index =False)
resDf.to_excel(writer, sheet_name='Resistors',index =False)
writer.save()

capsDf.loc[vaultCaps,'Footprint'] = capsDf.loc[vaultCaps,'Footprint'].str.partition(')')[0]
capsDf.loc[vaultCaps,'Footprint'] = capsDf.loc[vaultCaps,'Footprint'] + ')'
resDf.loc[vaultRs,'Footprint'] = resDf.loc[vaultRs,'Footprint'].str[:-2]

writer = pandas.ExcelWriter('processed BOM final.xlsx', engine='xlsxwriter')
BOM.to_excel(writer, sheet_name='Main',index =False)
capsDf.to_excel(writer, sheet_name='Caps',index =False)
connDf.to_excel(writer, sheet_name='Connectors',index =False)
indDf.to_excel(writer, sheet_name='Ind',index =False)
resDf.to_excel(writer, sheet_name='Resistors',index =False)
writer.save()

#print(BOM.columns.astype(str).values.tolist())
#writer = StyleFrame.ExcelWriter('processed BOM.xlsx')
#StyleFrame(BOM).to_excel(writer, sheet_name='Main',index =False,best_fit=['Manufacturer', 'ManufacturerNr', 'Quantity', 'Designator'])
#capsDf.to_excel(writer, sheet_name='Caps',index =False)
#indDf.to_excel(writer, sheet_name='Ind',index =False)
#resDf.to_excel(writer, sheet_name='Resistors',index =False)
#writer.save()